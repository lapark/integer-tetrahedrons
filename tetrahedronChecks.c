/*******************************************************************
 * tetrahedronChecks
 * Author: Laurence Park, 2020
 *
 *
 *
 *******************************************************************/

#include "tetrahedronChecks.h"

#include <stdio.h>

#ifdef PRECISION
#include <gmp.h>
#endif


#define TRUE  1
#define FALSE 0


/* See https://en.wikipedia.org/wiki/C_data_types for details on data type precision. */

long double Square(long double x) {
  return(x*x);
}

long long ISquare(long long x) {
  return(x*x);
}


//https://cs.uwaterloo.ca/journals/JIS/VOL10/Kurz/kurz3.pdf
int checkCanonicalVector(long long *D) {
  if (D[0] == D[1]) {
    if (D[1] == D[2]) {
      if (D[3] < D[4]) {
	return(FALSE);
      }
      else {
	if (D[4] < D[5]) {
	  return(FALSE);
	}
	else {
	  return(TRUE);
	}
      }
    }
    else {
      if (D[4] < D[5]) {
	return(FALSE);
      }
      else {
	if (D[0] == D[3]) {
	  if (D[2] < D[4]) {
	    return(FALSE);
	  }
	  else {
	    return(TRUE);
	  }
	}
	else{
	  if (D[0] > D[4]) {
	    return(TRUE);
	  }
	  else {
	    if (D[2] < D[3]) {
	      return(FALSE);
	    }
	    else {
	      return(TRUE);
	    }
	  }
	}
      }
    }
  }
  else {
    if (D[1] == D[2]) {
      if ((D[0] < D[5]) | (D[3] < D[4])) {
	return(FALSE);
      }
      else {
	return(TRUE);
      }
    }
    else {
      if (D[1] == D[4]) {
	if ((D[3] > D[2]) | (D[0] < D[5])) {
	  return(FALSE);
	}
	else {
	  return(TRUE);
	}
      }
      else {
	if (D[1] == D[3]) {
	  if ((D[2] < D[4]) | (D[0] <= D[5])) {
	    return(FALSE);
	  }
	  else {
	    return(TRUE);
	  }
	}
	else {
	  if (D[3] > D[2]) {
	    if (D[0] > D[5]) {
	      //if (D[0] <= D[5]) {
	      return(TRUE);
	    }
	    else {
	      return(FALSE);
	    }
	  }
	  else {
	    if (D[0] >= D[5]) {
	      //if (D[0] < D[5]) {
	      return(TRUE);
	    }
	    else {
	      return(FALSE);
	    }
	  }
	}
      }
    }
  }
}




unsigned long long countTetrahedronDiameter(long long tetrahedronSize, long long D01) {

  unsigned long long counter = 0;
  long long D[6];
  //double D[6];
  D[0] = D01;

  //long double x1, x2, z1, z2;
  //long double x1, x2, Y1, Y2;
  long long x1, x2, z1, z2;
  
  for (D[1] = (D[0] + 2)/2; D[1] <= D[0]; D[1]++) {
    for (D[2] = D[0] + 1 - D[1]; D[2] <= D[1]; D[2]++) {	    
      for (D[3] = D[0] + 1 - D[1]; D[3] <= D[1]; D[3]++) {
	for (D[4] = D[0] + 1 - D[3]; D[4] <= D[1]; D[4]++) {
	  D[5] = tetrahedronSize - D[0] - D[1] - D[2] - D[3] - D[4];
	  if ((D[5] > 0) & (D[5] <= D[0])) {
	    
	    x1 = ISquare(D[0]) + ISquare(D[1]) - ISquare(D[2]);
	    z1 = 4*ISquare(D[0])*ISquare(D[1]) - ISquare(x1);

	    x2 = ISquare(D[0]) + ISquare(D[3]) - ISquare(D[4]);
	    z2 = 4*ISquare(D[0])*ISquare(D[3]) - ISquare(x2);

	    //x1 = (Square(D[0]) + Square(D[1]) - Square(D[2]))/(2*D[0]);
	    //x2 = (Square(D[0]) + Square(D[3]) - Square(D[4]))/(2*D[0]);
	    //Y1 = Square(D[1]) - Square(x1);
	    //Y2 = Square(D[3]) - Square(x2);

	    //if (Square(Square(D[5]) - Square(D[1]) - Square(D[3]) + 2*x1*x2) < (4*Y1*Y2)) {
	    if (ISquare(2*ISquare(D[0])*ISquare(D[5]) - 2*ISquare(D[0])*ISquare(D[1]) - 2*ISquare(D[0])*ISquare(D[3]) + x1*x2) < (z1*z2)) {
	      if (checkCanonicalVector(D) != 0) {
		counter++;
		//fprintf(stdout, "T:%lli,%lli,%lli,%lli,%lli,%lli\n", D[0], D[1], D[2], D[3], D[4], D[5]);
		//fprintf(stdout, "T:%llu,%llu,%llu,%llu\n", D[1], D[2], D[3], D[4]);
	      }
	    }
	  }
	}
      }
    }
  }
  return(counter);
}


unsigned long long countTetrahedronDiameterZdouble(long long tetrahedronSize, long long D01) {

  unsigned long long counter = 0;
  long long D[6];
  long long DS[6];
  D[0] = D01;
  DS[0] = ISquare(D[0]);

  long long x1, x2;
  long double z1, z2, lhs, rhs;
  
  for (D[1] = (D[0] + 2)/2; D[1] <= D[0]; D[1]++) {
    for (D[2] = D[0] + 1 - D[1]; D[2] <= D[1]; D[2]++) {	    
      for (D[3] = D[0] + 1 - D[1]; D[3] <= D[1]; D[3]++) {
	for (D[4] = D[0] + 1 - D[3]; D[4] <= D[1]; D[4]++) {
	  D[5] = tetrahedronSize - D[0] - D[1] - D[2] - D[3] - D[4];
	  if ((D[5] > 0) & (D[5] <= D[0])) {
	    
	    DS[1] = ISquare(D[1]);
	    DS[2] = ISquare(D[2]);
	    DS[3] = ISquare(D[3]);
	    DS[4] = ISquare(D[4]);
	    DS[5] = ISquare(D[5]);
	    
	    x1 = DS[0] + DS[1] - DS[2];
	    x2 = DS[0] + DS[3] - DS[4];

	    z1 = 4*DS[0]*DS[1] - ISquare(x1);
	    z2 = 4*DS[0]*DS[3] - ISquare(x2);
	    
	    if (Square((long double) 2*DS[0]*(DS[5] - DS[1] - DS[3]) + x1*x2) < (z1*z2)) {
	      if (checkCanonicalVector(D) != 0) {
		counter++;
		//fprintf(stdout, "T:%lli,%lli,%lli,%lli,%lli,%lli\n", D[0], D[1], D[2], D[3], D[4], D[5]);
		//fprintf(stdout, "T:%llu,%llu,%llu,%llu\n", D[1], D[2], D[3], D[4]);
	      }
	    }
	  }
	}
      }
    }
  }
  return(counter);
}

unsigned long long countTetrahedronDiameterDouble(long long tetrahedronSize, long long D01) {

  unsigned long long counter = 0;
  long long D[6];
  //double D[6];
  D[0] = D01;

  long double x1, x2, Y1, Y2;
  long double delta = 1e-10;
  
  for (D[1] = (D[0] + 2)/2; D[1] <= D[0]; D[1]++) {
    for (D[2] = D[0] + 1 - D[1]; D[2] <= D[1]; D[2]++) {
      for (D[3] = D[0] + 1 - D[1]; D[3] <= D[1]; D[3]++) {
	for (D[4] = D[0] + 1 - D[3]; D[4] <= D[1]; D[4]++) {
	  D[5] = tetrahedronSize - D[0] - D[1] - D[2] - D[3] - D[4];
	  if ((D[5] > 0) & (D[5] <= D[0])) {
	    x1 = ((long double) (ISquare(D[0]) + ISquare(D[1]) - ISquare(D[2])))/(2*D[0]);
	    x2 = ((long double) (ISquare(D[0]) + ISquare(D[3]) - ISquare(D[4])))/(2*D[0]);
	    Y1 = ((long double) ISquare(D[1])) - Square(x1);
	    Y2 = ((long double) ISquare(D[3])) - Square(x2);

	    if (Square(((long double) ISquare(D[5]) - ISquare(D[1]) - ISquare(D[3])) + 2*x1*x2) < (4*Y1*Y2 - delta)) {
	      if (checkCanonicalVector(D) != 0) {
		counter++;
		//fprintf(stdout, "T:%lli,%lli,%lli,%lli,%lli,%lli\n", D[0], D[1], D[2], D[3], D[4], D[5]);
		//fprintf(stdout, "T:%llu,%llu,%llu,%llu\n", D[1], D[2], D[3], D[4]);
	      }
	    }
	  }
	}
      }
    }
  }
  return(counter);
}

#ifdef PRECISION
unsigned long long countTetrahedronDiameterHybrid(long long tetrahedronSize, long long D01) {

  unsigned long long counter = 0;
  long long D[6];
  long long DS[6];
  D[0] = D01;
  DS[0] = ISquare(D[0]);

  long double x1, x2, z1, z2, check;
  
  mpz_t mpz1, mpz2, mpx1, mpx2, mplhs, mprhs;
  mpz_init2(mpz1,128);
  mpz_init2(mpz2,128);
  mpz_init2(mpx1,128);
  mpz_init2(mpx2,128);
  mpz_init2(mplhs,128);
  mpz_init2(mprhs,128);


  for (D[1] = (D[0] + 2)/2; D[1] <= D[0]; D[1]++) {
    for (D[2] = D[0] + 1 - D[1]; D[2] <= D[1]; D[2]++) {	    
      for (D[3] = D[0] + 1 - D[1]; D[3] <= D[1]; D[3]++) {
	for (D[4] = D[0] + 1 - D[3]; D[4] <= D[1]; D[4]++) {
	  D[5] = tetrahedronSize - D[0] - D[1] - D[2] - D[3] - D[4];
	  if ((D[5] > 0) & (D[5] <= D[0])) {
	    
	    DS[1] = ISquare(D[1]);
	    DS[2] = ISquare(D[2]);
	    DS[3] = ISquare(D[3]);
	    DS[4] = ISquare(D[4]);
	    DS[5] = ISquare(D[5]);
	    
	    
	    x1 = DS[0] + DS[1] - DS[2];
	    z1 = 4*DS[0]*DS[1] - ISquare(x1);
	    
	    //x2 = ISquare(D[0]) + ISquare(D[3]) - ISquare(D[4]);
	    //z2 = 4*ISquare(D[0])*ISquare(D[3]) - ISquare(x2);
	    x2 = DS[0] + DS[3] - DS[4];
	    z2 = 4*DS[0]*DS[3] - ISquare(x2);

	    //check = Square(2*ISquare(D[0])*ISquare(D[5]) - 2*ISquare(D[0])*ISquare(D[1]) - 2*ISquare(D[0])*ISquare(D[3]) + x1*x2) - (z1*z2);
	    //check = Square(2*ISquare(D[0])*(ISquare(D[5]) - ISquare(D[1]) - ISquare(D[3])) + x1*x2) - (z1*z2);
	    check = Square(2*DS[0]*(DS[5] - DS[1] - DS[3]) + x1*x2) - (z1*z2);

	    if (check == 0) {

	      // set x1 and x2
	      mpz_init_set_si(mpx1, x1);
	      mpz_init_set_si(mpx2, x2);
	    
	      // set z1
	      mpz_init_set_si(mpz1, 4*DS[0]);
	      mpz_mul_si(mpz1, mpz1, DS[1]);
	      mpz_submul(mpz1, mpx1, mpx1);

	      // set z2
	      mpz_init_set_si(mpz2, 4*DS[0]);
	      mpz_mul_si(mpz2, mpz2, DS[3]);
	      mpz_submul(mpz2, mpx2, mpx2);
	      
	      mpz_init_set_si(mplhs, DS[5] - DS[1] - DS[3]);
	      mpz_mul_si(mplhs, mplhs, 2*DS[0]);
	      mpz_addmul(mplhs, mpx1, mpx2);
	      mpz_mul(mplhs, mplhs, mplhs);
	    
	      mpz_init_set_si(mprhs, z1);
	      mpz_mul_si(mprhs, mprhs, z2);
	    
	      if (mpz_cmp(mplhs, mprhs) < 0) {
		fprintf(stdout, "\nT:%lli,%lli,%lli,%lli,%lli,%lli\n", D[0], D[1], D[2], D[3], D[4], D[5]);
		if (checkCanonicalVector(D) != 0) {
		  counter++;
		}
	      }
	    }
	    if (check < 0) {
	      if (checkCanonicalVector(D) != 0) {
		counter++;
		//fprintf(stdout, "T:%lli,%lli,%lli,%lli,%lli,%lli\n", D[0], D[1], D[2], D[3], D[4], D[5]);
		//fprintf(stdout, "T:%llu,%llu,%llu,%llu\n", D[1], D[2], D[3], D[4]);
	      }
	    }
	  }
	}
      }
    }
  }

  mpz_clear(mpz1);
  mpz_clear(mpz2);
  mpz_clear(mpx1);
  mpz_clear(mpx2);
  mpz_clear(mplhs);
  mpz_clear(mprhs);

  return(counter);
}

unsigned long long countTetrahedronDiameterHybridCanonFirst(long long tetrahedronSize, long long D01) {

  unsigned long long counter = 0;
  long long D[6];
  long long DS[6];
  D[0] = D01;
  DS[0] = ISquare(D[0]);

  long double x1, x2, z1, z2, check;
  
  mpz_t mpz1, mpz2, mpx1, mpx2, mplhs, mprhs;
  mpz_init2(mpz1,128);
  mpz_init2(mpz2,128);
  mpz_init2(mpx1,128);
  mpz_init2(mpx2,128);
  mpz_init2(mplhs,128);
  mpz_init2(mprhs,128);


  for (D[1] = (D[0] + 2)/2; D[1] <= D[0]; D[1]++) {
    for (D[2] = D[0] + 1 - D[1]; D[2] <= D[1]; D[2]++) {	    
      for (D[3] = D[0] + 1 - D[1]; D[3] <= D[1]; D[3]++) {
	for (D[4] = D[0] + 1 - D[3]; D[4] <= D[1]; D[4]++) {
	  D[5] = tetrahedronSize - D[0] - D[1] - D[2] - D[3] - D[4];
	  if ((D[5] > 0) & (D[5] <= D[0])) {
	    if (checkCanonicalVector(D) != 0) {
	      
	      DS[1] = ISquare(D[1]);
	      DS[2] = ISquare(D[2]);
	      DS[3] = ISquare(D[3]);
	      DS[4] = ISquare(D[4]);
	      DS[5] = ISquare(D[5]);
	      
	      
	      x1 = DS[0] + DS[1] - DS[2];
	      z1 = 4*DS[0]*DS[1] - ISquare(x1);
	    
	      //x2 = ISquare(D[0]) + ISquare(D[3]) - ISquare(D[4]);
	      //z2 = 4*ISquare(D[0])*ISquare(D[3]) - ISquare(x2);
	      x2 = DS[0] + DS[3] - DS[4];
	      z2 = 4*DS[0]*DS[3] - ISquare(x2);
	      
	      //check = Square(2*ISquare(D[0])*ISquare(D[5]) - 2*ISquare(D[0])*ISquare(D[1]) - 2*ISquare(D[0])*ISquare(D[3]) + x1*x2) - (z1*z2);
	      //check = Square(2*ISquare(D[0])*(ISquare(D[5]) - ISquare(D[1]) - ISquare(D[3])) + x1*x2) - (z1*z2);
	      check = Square(2*DS[0]*(DS[5] - DS[1] - DS[3]) + x1*x2) - (z1*z2);
	      
	      if (check == 0) {
		
		// set x1 and x2
		mpz_init_set_si(mpx1, x1);
		mpz_init_set_si(mpx2, x2);
		
		// set z1
		mpz_init_set_si(mpz1, 4*DS[0]);
		mpz_mul_si(mpz1, mpz1, DS[1]);
		mpz_submul(mpz1, mpx1, mpx1);
		
		// set z2
		mpz_init_set_si(mpz2, 4*DS[0]);
		mpz_mul_si(mpz2, mpz2, DS[3]);
		mpz_submul(mpz2, mpx2, mpx2);
		
		mpz_init_set_si(mplhs, DS[5] - DS[1] - DS[3]);
		mpz_mul_si(mplhs, mplhs, 2*DS[0]);
		mpz_addmul(mplhs, mpx1, mpx2);
		mpz_mul(mplhs, mplhs, mplhs);
		
		mpz_init_set_si(mprhs, z1);
		mpz_mul_si(mprhs, mprhs, z2);
		
		if (mpz_cmp(mplhs, mprhs) < 0) {
		  fprintf(stdout, "\nT:%lli,%lli,%lli,%lli,%lli,%lli\n", D[0], D[1], D[2], D[3], D[4], D[5]);
		  counter++;
		}
	      }
	      if (check < 0) {
		counter++;
		//fprintf(stdout, "T:%lli,%lli,%lli,%lli,%lli,%lli\n", D[0], D[1], D[2], D[3], D[4], D[5]);
		//fprintf(stdout, "T:%llu,%llu,%llu,%llu\n", D[1], D[2], D[3], D[4]);
	      }
	    }
	  }
	}
      }
    }
  }
  
  mpz_clear(mpz1);
  mpz_clear(mpz2);
  mpz_clear(mpx1);
  mpz_clear(mpx2);
  mpz_clear(mplhs);
  mpz_clear(mprhs);

  return(counter);
}

unsigned long long countTetrahedronDiameterHybridMultiCheck(long long tetrahedronSize, long long D01) {

  unsigned long long counter = 0;
  long long D[6];
  long long DS[6];
  D[0] = D01;
  DS[0] = ISquare(D[0]);

  long double x1, x2, z1, z2, check;
  long double check1, check2, check3;
  
  mpz_t mpz1, mpz2, mpx1, mpx2, mplhs, mprhs;
  mpz_init2(mpz1,128);
  mpz_init2(mpz2,128);
  mpz_init2(mpx1,128);
  mpz_init2(mpx2,128);
  mpz_init2(mplhs,128);
  mpz_init2(mprhs,128);


  for (D[1] = (D[0] + 2)/2; D[1] <= D[0]; D[1]++) {

    DS[1] = ISquare(D[1]);
	    
    for (D[2] = D[0] + 1 - D[1]; D[2] <= D[1]; D[2]++) {	    

      DS[2] = ISquare(D[2]);
      x1 = DS[0] + DS[1] - DS[2];
      z1 = 4*DS[0]*DS[1] - ISquare(x1);
      
      for (D[3] = D[0] + 1 - D[1]; D[3] <= D[1]; D[3]++) {

	DS[3] = ISquare(D[3]);

	for (D[4] = D[0] + 1 - D[3]; D[4] <= D[1]; D[4]++) {

	  DS[4] = ISquare(D[4]);
	  x2 = DS[0] + DS[3] - DS[4];
	  z2 = 4*DS[0]*DS[3] - ISquare(x2);
	  
	  D[5] = tetrahedronSize - D[0] - D[1] - D[2] - D[3] - D[4];
	  DS[5] = ISquare(D[5]);
	    
	  if ((D[5] > 0) & (D[5] <= D[0])) {
	    
	    //check = Square(2*DS[0]*(DS[5] - DS[1] - DS[3]) + x1*x2) - (z1*z2);

	    check1 = Square(2*DS[0]*(DS[5] - DS[1] - DS[3]) + x1*x2);
	    check2 = (z1*z2);
	    check3 = check2 + 1;

	    if ((check1 == check2) && (check1 == check3)) {
	      // problem with precision!

	      // set x1 and x2
	      mpz_init_set_si(mpx1, x1);
	      mpz_init_set_si(mpx2, x2);
	    
	      // set z1
	      mpz_init_set_si(mpz1, 4*DS[0]);
	      mpz_mul_si(mpz1, mpz1, DS[1]);
	      mpz_submul(mpz1, mpx1, mpx1);

	      // set z2
	      mpz_init_set_si(mpz2, 4*DS[0]);
	      mpz_mul_si(mpz2, mpz2, DS[3]);
	      mpz_submul(mpz2, mpx2, mpx2);
	      
	      mpz_init_set_si(mplhs, DS[5] - DS[1] - DS[3]);
	      mpz_mul_si(mplhs, mplhs, 2*DS[0]);
	      mpz_addmul(mplhs, mpx1, mpx2);
	      mpz_mul(mplhs, mplhs, mplhs);
	    
	      mpz_init_set_si(mprhs, z1);
	      mpz_mul_si(mprhs, mprhs, z2);
	    
	      if (mpz_cmp(mplhs, mprhs) < 0) {
		fprintf(stdout, "\nT:%lli,%lli,%lli,%lli,%lli,%lli\n", D[0], D[1], D[2], D[3], D[4], D[5]);
		if (checkCanonicalVector(D) != 0) {
		  counter++;
		}
	      }
	    }
	    if (check1 < check2) {
	      if (checkCanonicalVector(D) != 0) {
		counter++;
		//fprintf(stdout, "T:%lli,%lli,%lli,%lli,%lli,%lli\n", D[0], D[1], D[2], D[3], D[4], D[5]);
		//fprintf(stdout, "T:%llu,%llu,%llu,%llu\n", D[1], D[2], D[3], D[4]);
	      }
	    }
	  }
	}
      }
    }
  }

  mpz_clear(mpz1);
  mpz_clear(mpz2);
  mpz_clear(mpx1);
  mpz_clear(mpx2);
  mpz_clear(mplhs);
  mpz_clear(mprhs);

  return(counter);
}

// http://mathworld.wolfram.com/Cayley-MengerDeterminant.html
// https://www.mathpages.com/home/kmath664/kmath664.htm
long double dellaFrancesca(const long long *DS) {
  
  long double DX[9];
  
  DX[0] = DS[0]*DS[5]; //af2 = DS[0]*DS[5];
  DX[1] = DS[1]*DS[4]; // be2 = DS[1]*DS[4];
  DX[2] = DS[2]*DS[3]; // cd2 = DS[2]*DS[3];
  DX[3] = DS[0] + DS[5]; // apf2 = DS[0] + DS[5];
  DX[4] = DS[1] + DS[4]; // bpe2 = DS[1] + DS[4];
  DX[5] = DS[2] + DS[3]; // cpd2 = DS[2] + DS[3];
  DX[6] = DS[0] - DS[5]; // amf2 = DS[0] - DS[5];
  DX[7] = DS[1] - DS[4]; // bme2 = DS[1] - DS[4];
  //DX[8] = DS[2] - DS[3]; // cmd2 = DS[2] - DS[3];
  DX[8] = DS[3] - DS[2]; // cmd2 = DS[2] - DS[3];


  return((2*(DS[0] + DS[1] + DS[2] + DS[3] + DS[4] + DS[5])*(DX[0] + DX[1] + DX[2]) +
	  DX[6]*DX[7]*DX[8]
	  - (4*(DX[0]*DX[3] + DX[1]*DX[4] + DX[2]*DX[5]) + DX[3]*DX[4]*DX[5])));

  /*  
  return((2*(DS[0] + DS[1] + DS[2] + DS[3] + DS[4] + DS[5])*(DX[0] + DX[1] + DX[2]) +
	  DX[6]*DX[7]*DX[8]));
  */
}

long double jamesEastC6(const long long *DS) {

  long double x1 = DS[0] + DS[1] - DS[2];
  long double z1 = 4*DS[0]*DS[1] - ISquare(x1);

  long double x2 = DS[0] + DS[3] - DS[4];
  long double z2 = 4*DS[0]*DS[3] - ISquare(x2);

  return((z1*z2) - Square(2*DS[0]*(DS[5] - DS[1] - DS[3]) + x1*x2));

  //return((z1*z2));
}


unsigned long long countTetrahedronDiameterHybridMultiCheckCanonFirst(long long tetrahedronSize, long long D01) {

  unsigned long long counter = 0;
  long long D[6];
  long long DS[6];
  D[0] = D01;
  DS[0] = ISquare(D[0]);

  int a;
  
  long double x1, x2, z1, z2, check;
  long double check1, check2, check3;

#ifdef CHECK_LARGEST
  long long longestD[6];
  for (a = 0; a < 6; a++) {
    longestD[a] = 0;
  }
#endif
  
  mpz_t mpz1, mpz2, mpx1, mpx2, mplhs, mprhs;
  mpz_init2(mpz1,128);
  mpz_init2(mpz2,128);
  mpz_init2(mpx1,128);
  mpz_init2(mpx2,128);
  mpz_init2(mplhs,128);
  mpz_init2(mprhs,128);

#ifdef CHECK_LARGEST  
  mpz_t largestLHS, largestRHS;
  mpz_init2(largestLHS,128);
  mpz_init2(largestRHS,128);

  mpz_init_set_si(largestLHS, 0);
  mpz_init_set_si(largestRHS, 0);
#endif
  
  for (D[1] = (D[0] + 2)/2; D[1] <= D[0]; D[1]++) {

    DS[1] = ISquare(D[1]);
	    
    for (D[2] = D[0] + 1 - D[1]; D[2] <= D[1]; D[2]++) {	    

      DS[2] = ISquare(D[2]);
      x1 = DS[0] + DS[1] - DS[2];
      z1 = 4*DS[0]*DS[1] - ISquare(x1);
      
      for (D[3] = D[0] + 1 - D[1]; D[3] <= D[1]; D[3]++) {

	DS[3] = ISquare(D[3]);

	for (D[4] = D[0] + 1 - D[3]; D[4] <= D[1]; D[4]++) {

	  DS[4] = ISquare(D[4]);
	  x2 = DS[0] + DS[3] - DS[4];
	  z2 = 4*DS[0]*DS[3] - ISquare(x2);
	  
	  D[5] = tetrahedronSize - D[0] - D[1] - D[2] - D[3] - D[4];
	  DS[5] = ISquare(D[5]);
	    
	  if ((D[5] > 0) & (D[5] <= D[0])) {
	    
	    if (checkCanonicalVector(D) != 0) {
	      //check = Square(2*DS[0]*(DS[5] - DS[1] - DS[3]) + x1*x2) - (z1*z2);

	      check1 = Square(2*DS[0]*(DS[5] - DS[1] - DS[3]) + x1*x2);
	      check2 = (z1*z2);
	      check3 = check2 + 1;
	      
	      if ((check1 == check2) && (check1 == check3)) {
		// problem with precision!
		
		// set x1 and x2
		mpz_init_set_si(mpx1, x1);
		mpz_init_set_si(mpx2, x2);
		
		// set z1
		mpz_init_set_si(mpz1, 4*DS[0]);
		mpz_mul_si(mpz1, mpz1, DS[1]);
		mpz_submul(mpz1, mpx1, mpx1);
		
		// set z2
		mpz_init_set_si(mpz2, 4*DS[0]);
		mpz_mul_si(mpz2, mpz2, DS[3]);
		mpz_submul(mpz2, mpx2, mpx2);
		
		mpz_init_set_si(mplhs, DS[5] - DS[1] - DS[3]);
		mpz_mul_si(mplhs, mplhs, 2*DS[0]);
		mpz_addmul(mplhs, mpx1, mpx2);
		mpz_mul(mplhs, mplhs, mplhs);
		
		mpz_init_set_si(mprhs, z1);
		mpz_mul_si(mprhs, mprhs, z2);

#ifdef CHECK_LARGEST		
		if (mpz_cmp(mplhs, largestLHS) > 0) {
		  mpz_init_set(largestLHS, mplhs);		  
		}
		if (mpz_cmp(mprhs, largestRHS) > 0) {
		  mpz_init_set(largestRHS, mprhs);		  
		}

		for (a = 0; a < 6; a++) {
		  longestD[a] = D[a];
		}
#endif
		
		if (mpz_cmp(mplhs, mprhs) < 0) {
		  fprintf(stdout, "\nT:%lli,%lli,%lli,%lli,%lli,%lli\n", D[0], D[1], D[2], D[3], D[4], D[5]);
		  counter++;
		}
#ifdef CHECK_DIFFERENCE
		mpz_sub(mprhs, mprhs, mplhs);
		fprintf(stdout, "%Zd,", mprhs);
#endif		
		
	      }
	      else if (check1 < check2) {
		counter++;
#ifdef CHECK_DIFFERENCE		
		fprintf(stdout, "%Lf,", check2 - check1);
#endif		  
		  //fprintf(stdout, "T:%lli,%lli,%lli,%lli,%lli,%lli\n", D[0], D[1], D[2], D[3], D[4], D[5]);
		//fprintf(stdout, "T:%llu,%llu,%llu,%llu\n", D[1], D[2], D[3], D[4]);
	      }
#ifdef CHECK_DIFFERENCE	      
	      else {
		fprintf(stdout, "%Lf,", check2 - check1);
	      }
#endif	      
	    }
	  }
	}
      }
    }
  }

#ifdef CHECK_LARGEST
  gmp_fprintf(stderr, "\n## stats: %lli, %Zd, %Zd, : %lli,%lli,%lli,%lli,%lli,%lli\n", D[0], largestLHS, largestRHS, longestD[0], longestD[1], longestD[2], longestD[3], longestD[4], longestD[5]);

  mpz_clear(largestLHS);
  mpz_clear(largestRHS);
#endif
  
  mpz_clear(mpz1);
  mpz_clear(mpz2);
  mpz_clear(mpx1);
  mpz_clear(mpx2);
  mpz_clear(mplhs);
  mpz_clear(mprhs);

  return(counter);
}


unsigned long long countTetrahedronDiameterBasic(long long tetrahedronSize, long long D01) {

  unsigned long long counter = 0;
  long long D[6];
  long long DS[6];
  D[0] = D01;
  DS[0] = ISquare(D[0]);

  long double della, james;

  
  for (D[1] = (D[0] + 2)/2; D[1] <= D[0]; D[1]++) {

    DS[1] = ISquare(D[1]);
	    
    for (D[2] = D[0] + 1 - D[1]; D[2] <= D[1]; D[2]++) {	    

      DS[2] = ISquare(D[2]);
      
      for (D[3] = D[0] + 1 - D[1]; D[3] <= D[1]; D[3]++) {

	DS[3] = ISquare(D[3]);

	for (D[4] = D[0] + 1 - D[3]; D[4] <= D[1]; D[4]++) {

	  DS[4] = ISquare(D[4]);
	  
	  D[5] = tetrahedronSize - D[0] - D[1] - D[2] - D[3] - D[4];
	  DS[5] = ISquare(D[5]);
	    
	  if ((D[5] > 0) & (D[5] <= D[0])) {
	    
	    if (checkCanonicalVector(D) != 0) {
	      //check = Square(2*DS[0]*(DS[5] - Ds[1] - DS[3]) + x1*x2) - (z1*z2);

	      //della = dellaFrancesca(DS);
	      //james = jamesEastC6(DS);
	      if (dellaFrancesca(DS) > 0) {
	      //if (jamesEastC6(DS) > 0) {
		counter++;
		//fprintf(stdout, "K,%Lf,%Lf\n", james, della);
	      //fprintf(stdout, "T:%lli,%lli,%lli,%lli,%lli,%lli\n", D[0], D[1], D[2], D[3], D[4], D[5]);
	      }
	    }
	  }
	}
      }
    }
  }

  return(counter);
}


unsigned long long countTetrahedronDiameterOnlyCanon(long long tetrahedronSize, long long D01) {

  unsigned long long counter = 0;
  long long D[6];
  long long DS[6];
  D[0] = D01;
  DS[0] = ISquare(D[0]);

  int a;
  
  long double x1, x2, z1, z2, check;
  long double check1, check2, check3;

  
  for (D[1] = (D[0] + 2)/2; D[1] <= D[0]; D[1]++) {

    DS[1] = ISquare(D[1]);
	    
    for (D[2] = D[0] + 1 - D[1]; D[2] <= D[1]; D[2]++) {	    

      DS[2] = ISquare(D[2]);
      x1 = DS[0] + DS[1] - DS[2];
      z1 = 4*DS[0]*DS[1] - ISquare(x1);
      
      for (D[3] = D[0] + 1 - D[1]; D[3] <= D[1]; D[3]++) {

	DS[3] = ISquare(D[3]);

	for (D[4] = D[0] + 1 - D[3]; D[4] <= D[1]; D[4]++) {

	  DS[4] = ISquare(D[4]);
	  x2 = DS[0] + DS[3] - DS[4];
	  z2 = 4*DS[0]*DS[3] - ISquare(x2);
	  
	  D[5] = tetrahedronSize - D[0] - D[1] - D[2] - D[3] - D[4];
	  DS[5] = ISquare(D[5]);
	    
	  if ((D[5] > 0) & (D[5] <= D[0])) {
	    
	    if (checkCanonicalVector(D) != 0) {
	      //check = Square(2*DS[0]*(DS[5] - DS[1] - DS[3]) + x1*x2) - (z1*z2);

	      counter++;
		//fprintf(stdout, "T:%lli,%lli,%lli,%lli,%lli,%lli\n", D[0], D[1], D[2], D[3], D[4], D[5]);
		//fprintf(stdout, "T:%llu,%llu,%llu,%llu\n", D[1], D[2], D[3], D[4]);
	    }
	  }
	}
      }
    }
  }
  
  return(counter);
}


/* 
unsigned long long countTetrahedronDiameterAllPerimeterHybrid(long long D01) {

  unsigned long long counter = 0;
  long long D[6];
  long long DS[6];
  D[0] = D01;
  DS[0] = ISquare(D[0]);

  long double x1, x2, z1, z2, check;
  long long D5lower = 0;
  long long D5upper = 0;

  long long tetrahedronPerimeterLower = 3*D01 + 3;
  long long tetrahedronPerimeterUpper = 6*D01;
  long long perimeterCount = tetrahedronPerimeterUpper - tetrahedronPerimeterLower + 1;
  long long *tetrahedronPerimeters = malloc(sizeof(long long)*perimeterCount);
  
  int a = 0;
  int D5checkLower = 0;
  int D5checkUpper = 0;
  
  mpz_t mpz1, mpz2, mpx1, mpx2, mplhs, mprhs;
  mpz_init2(mpz1,128);
  mpz_init2(mpz2,128);
  mpz_init2(mpx1,128);
  mpz_init2(mpx2,128);
  mpz_init2(mplhs,128);
  mpz_init2(mprhs,128);


  for (D[1] = (D[0] + 2)/2; D[1] <= D[0]; D[1]++) {
    for (D[2] = D[0] + 1 - D[1]; D[2] <= D[1]; D[2]++) {	    
      for (D[3] = D[0] + 1 - D[1]; D[3] <= D[1]; D[3]++) {
	for (D[4] = D[0] + 1 - D[3]; D[4] <= D[1]; D[4]++) {

	  D5lower = tetrahedronPerimeterLower - D[0] - D[1] - D[2] - D[3] - D[4];
	  D5upper = tetrahedronPerimeterUpper - D[0] - D[1] - D[2] - D[3] - D[4];

	  for (a = 0; a < 5; a++) {
	    D5checkLower = 0;
	    D5checkUpper = 0;
	    
	    if ((D[a]-1) > D5lower) {
	      D5checkLower = 1;
	    }
	    if ((D[a]-1) < D5upper) {
	      D5checkUpper = 1;
	    }
	    if (D5checkLower | D5checkUpper) {
	    D[5] = D[a]-1;
	    if (checkCanonicalVector(D) != 0) {

	    }

	    }
	    //D[5] = tetrahedronSize - D[0] - D[1] - D[2] - D[3] - D[4];
	  if ((D[5] > 0) & (D[5] <= D[0])) {
	    
	    DS[1] = ISquare(D[1]);
	    DS[2] = ISquare(D[2]);
	    DS[3] = ISquare(D[3]);
	    DS[4] = ISquare(D[4]);
	    DS[5] = ISquare(D[5]);
	    
	    
	    x1 = DS[0] + DS[1] - DS[2];
	    z1 = 4*DS[0]*DS[1] - ISquare(x1);
	    
	    //x2 = ISquare(D[0]) + ISquare(D[3]) - ISquare(D[4]);
	    //z2 = 4*ISquare(D[0])*ISquare(D[3]) - ISquare(x2);
	    x2 = DS[0] + DS[3] - DS[4];
	    z2 = 4*DS[0]*DS[3] - ISquare(x2);

	    //check = Square(2*ISquare(D[0])*ISquare(D[5]) - 2*ISquare(D[0])*ISquare(D[1]) - 2*ISquare(D[0])*ISquare(D[3]) + x1*x2) - (z1*z2);
	    //check = Square(2*ISquare(D[0])*(ISquare(D[5]) - ISquare(D[1]) - ISquare(D[3])) + x1*x2) - (z1*z2);
	    check = Square(2*DS[0]*(DS[5] - DS[1] - DS[3]) + x1*x2) - (z1*z2);

	    if (check == 0) {

	      // set x1 and x2
	      mpz_init_set_si(mpx1, x1);
	      mpz_init_set_si(mpx2, x2);
	    
	      // set z1
	      mpz_init_set_si(mpz1, 4*DS[0]);
	      mpz_mul_si(mpz1, mpz1, DS[1]);
	      mpz_submul(mpz1, mpx1, mpx1);

	      // set z2
	      mpz_init_set_si(mpz2, 4*DS[0]);
	      mpz_mul_si(mpz2, mpz2, DS[3]);
	      mpz_submul(mpz2, mpx2, mpx2);
	      
	      mpz_init_set_si(mplhs, DS[5] - DS[1] - DS[3]);
	      mpz_mul_si(mplhs, mplhs, 2*DS[0]);
	      mpz_addmul(mplhs, mpx1, mpx2);
	      mpz_mul(mplhs, mplhs, mplhs);
	    
	      mpz_init_set_si(mprhs, z1);
	      mpz_mul_si(mprhs, mprhs, z2);
	    
	      if (mpz_cmp(mplhs, mprhs) < 0) {
		fprintf(stdout, "\nT:%lli,%lli,%lli,%lli,%lli,%lli\n", D[0], D[1], D[2], D[3], D[4], D[5]);
		if (checkCanonicalVector(D) != 0) {
		  counter++;
		}
	      }
	    }
	    if (check < 0) {
	      if (checkCanonicalVector(D) != 0) {
		counter++;
		//fprintf(stdout, "T:%lli,%lli,%lli,%lli,%lli,%lli\n", D[0], D[1], D[2], D[3], D[4], D[5]);
		//fprintf(stdout, "T:%llu,%llu,%llu,%llu\n", D[1], D[2], D[3], D[4]);
	      }
	    }
	  }
	}
      }
    }
  }

  mpz_clear(mpz1);
  mpz_clear(mpz2);
  mpz_clear(mpx1);
  mpz_clear(mpx2);
  mpz_clear(mplhs);
  mpz_clear(mprhs);

  return(counter);
}

*/

#endif

#ifdef PRECISION
unsigned long long countTetrahedronDiameterMP(long long tetrahedronSize, long long D01) {

  unsigned long long counter = 0;
  long long D[6];
  //double D[6];
  long long DS[6];

  D[0] = D01;
  DS[0] = ISquare(D[0]);

  //long double x1, x2, z1, z2;
  //long double x1, x2, Y1, Y2;
  long long x1, x2, z1, z2;
  long long lhs, rhs;
  
  mpz_t mpz1, mpz2, mpx1, mpx2, mplhs, mprhs;
  mpz_init2(mpz1,128);
  mpz_init2(mpz2,128);
  mpz_init2(mpx1,128);
  mpz_init2(mpx2,128);
  mpz_init2(mplhs,128);
  mpz_init2(mprhs,128);


  for (D[1] = (D[0] + 2)/2; D[1] <= D[0]; D[1]++) {
    for (D[2] = D[0] + 1 - D[1]; D[2] <= D[1]; D[2]++) {	    
      for (D[3] = D[0] + 1 - D[1]; D[3] <= D[1]; D[3]++) {
	for (D[4] = D[0] + 1 - D[3]; D[4] <= D[1]; D[4]++) {
	  D[5] = tetrahedronSize - D[0] - D[1] - D[2] - D[3] - D[4];
	  if ((D[5] > 0) & (D[5] <= D[0])) {


	    DS[1] = ISquare(D[1]);
	    DS[2] = ISquare(D[2]);
	    DS[3] = ISquare(D[3]);
	    DS[4] = ISquare(D[4]);
	    DS[5] = ISquare(D[5]);

	    x2 = ISquare(D[0]) + ISquare(D[3]) - ISquare(D[4]);
	    x1 = ISquare(D[0]) + ISquare(D[1]) - ISquare(D[2]);

	    //z1 = 4*ISquare(D[0])*ISquare(D[1]) - ISquare(x1);
	    //z2 = 4*ISquare(D[0])*ISquare(D[3]) - ISquare(x2);
	    
	    // set x1 and x2
	    mpz_init_set_si(mpx1, x1);
	    mpz_init_set_si(mpx2, x2);
	    
	    // set z1
	    mpz_init_set_si(mpz1, 4*DS[0]);
	    mpz_mul_si(mpz1, mpz1, DS[1]);
	    mpz_submul(mpz1, mpx1, mpx1);

	    // set z2
	    mpz_init_set_si(mpz2, 4*DS[0]);
	    mpz_mul_si(mpz2, mpz2, DS[3]);
	    mpz_submul(mpz2, mpx2, mpx2);
	      
	    mpz_init_set_si(mplhs, DS[5] - DS[1] - DS[3]);
	    mpz_mul_si(mplhs, mplhs, 2*DS[0]);
	    mpz_addmul(mplhs, mpx1, mpx2);
	    mpz_mul(mplhs, mplhs, mplhs);
	    
	    mpz_init_set_si(mprhs, z1);
	    mpz_mul_si(mprhs, mprhs, z2);
	    
	    if (mpz_cmp(mplhs, mprhs) < 0) {
	      if (checkCanonicalVector(D) != 0) {
		counter++;
	      }
	    }

	  }
	}
      }
    }
  }



  mpz_clear(mpz1);
  mpz_clear(mpz2);
  mpz_clear(mpx1);
  mpz_clear(mpx2);
  mpz_clear(mplhs);
  mpz_clear(mprhs);
  
  return(counter);
}
#endif



/*
unsigned long long countTetrahedronDiameterHybridMultiCheckCanonFirstDiameter(long long tetrahedronSize, long long D01) {

  unsigned long long counter = 0;
  long long D[6];
  long long DS[6];
  D[0] = D01;
  DS[0] = ISquare(D[0]);

  int a;
  
  long double x1, x2, z1, z2, check;
  long double check1, check2, check3;

  int validEdges = FALSE;

  
#ifdef CHECK_LARGEST
  long long longestD[6];
  for (a = 0; a < 6; a++) {
    longestD[a] = 0;
  }
#endif
  
  mpz_t mpz1, mpz2, mpx1, mpx2, mplhs, mprhs;
  mpz_init2(mpz1,128);
  mpz_init2(mpz2,128);
  mpz_init2(mpx1,128);
  mpz_init2(mpx2,128);
  mpz_init2(mplhs,128);
  mpz_init2(mprhs,128);

#ifdef CHECK_LARGEST  
  mpz_t largestLHS, largestRHS;
  mpz_init2(largestLHS,128);
  mpz_init2(largestRHS,128);

  mpz_init_set_si(largestLHS, 0);
  mpz_init_set_si(largestRHS, 0);
#endif
  
  for (D[1] = (D[0] + 2)/2; D[1] <= D[0]; D[1]++) {

    DS[1] = ISquare(D[1]);
	    
    for (D[2] = D[0] + 1 - D[1]; D[2] <= D[1]; D[2]++) {	    

      DS[2] = ISquare(D[2]);
      x1 = DS[0] + DS[1] - DS[2];
      z1 = 4*DS[0]*DS[1] - ISquare(x1);
      
      for (D[3] = D[0] + 1 - D[1]; D[3] <= D[1]; D[3]++) {

	DS[3] = ISquare(D[3]);

	for (D[4] = D[0] + 1 - D[3]; D[4] <= D[1]; D[4]++) {

	  DS[4] = ISquare(D[4]);
	  x2 = DS[0] + DS[3] - DS[4];
	  z2 = 4*DS[0]*DS[3] - ISquare(x2);
	  
	  // here we compute the range of possible values <---
	  for (D[5] = + 1 - D[3]; D[4] <= D[1]; D[4]++) {

	  D[5] = tetrahedronSize - D[0] - D[1] - D[2] - D[3] - D[4];
	  DS[5] = ISquare(D[5]);
	    
	  if ((D[5] > 0) & (D[5] <= D[0])) {
	    
	    if (checkCanonicalVector(D) != 0) {
	      //check = Square(2*DS[0]*(DS[5] - DS[1] - DS[3]) + x1*x2) - (z1*z2);

	      check1 = Square(2*DS[0]*(DS[5] - DS[1] - DS[3]) + x1*x2);
	      check2 = (z1*z2);
	      check3 = check2 + 1;
	      
	      if ((check1 == check2) && (check1 == check3)) {
		// problem with precision!
		
		// set x1 and x2
		mpz_init_set_si(mpx1, x1);
		mpz_init_set_si(mpx2, x2);
		
		// set z1
		mpz_init_set_si(mpz1, 4*DS[0]);
		mpz_mul_si(mpz1, mpz1, DS[1]);
		mpz_submul(mpz1, mpx1, mpx1);
		
		// set z2
		mpz_init_set_si(mpz2, 4*DS[0]);
		mpz_mul_si(mpz2, mpz2, DS[3]);
		mpz_submul(mpz2, mpx2, mpx2);
		
		mpz_init_set_si(mplhs, DS[5] - DS[1] - DS[3]);
		mpz_mul_si(mplhs, mplhs, 2*DS[0]);
		mpz_addmul(mplhs, mpx1, mpx2);
		mpz_mul(mplhs, mplhs, mplhs);
		
		mpz_init_set_si(mprhs, z1);
		mpz_mul_si(mprhs, mprhs, z2);

#ifdef CHECK_LARGEST		
		if (mpz_cmp(mplhs, largestLHS) > 0) {
		  mpz_init_set(largestLHS, mplhs);		  
		}
		if (mpz_cmp(mprhs, largestRHS) > 0) {
		  mpz_init_set(largestRHS, mprhs);		  
		}

		for (a = 0; a < 6; a++) {
		  longestD[a] = D[a];
		}
#endif
		
		if (mpz_cmp(mplhs, mprhs) < 0) {
		  fprintf(stdout, "\nT:%lli,%lli,%lli,%lli,%lli,%lli\n", D[0], D[1], D[2], D[3], D[4], D[5]);
		  counter++;
		}
#ifdef CHECK_DIFFERENCE
		mpz_sub(mprhs, mprhs, mplhs);
		fprintf(stdout, "%Zd,", mprhs);
#endif		
		
	      }
	      else if (check1 < check2) {
		counter++;
#ifdef CHECK_DIFFERENCE		
		fprintf(stdout, "%Lf,", check2 - check1);
#endif		  
		  //fprintf(stdout, "T:%lli,%lli,%lli,%lli,%lli,%lli\n", D[0], D[1], D[2], D[3], D[4], D[5]);
		//fprintf(stdout, "T:%llu,%llu,%llu,%llu\n", D[1], D[2], D[3], D[4]);
	      }
#ifdef CHECK_DIFFERENCE	      
	      else {
		fprintf(stdout, "%Lf,", check2 - check1);
	      }
#endif	      
	    }
	  }
	}
      }
    }
  }

#ifdef CHECK_LARGEST
  gmp_fprintf(stderr, "\n## stats: %lli, %Zd, %Zd, : %lli,%lli,%lli,%lli,%lli,%lli\n", D[0], largestLHS, largestRHS, longestD[0], longestD[1], longestD[2], longestD[3], longestD[4], longestD[5]);

  mpz_clear(largestLHS);
  mpz_clear(largestRHS);
#endif
  
  mpz_clear(mpz1);
  mpz_clear(mpz2);
  mpz_clear(mpx1);
  mpz_clear(mpx2);
  mpz_clear(mplhs);
  mpz_clear(mprhs);

  return(counter);
}
*/
