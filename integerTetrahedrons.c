/* 
   Compute the number of integer length tetrahedrons.
   Compile using gcc -Ofast -o integerTetrahedrons integerTetrahedrons.c
gcc -I/opt/local/include/ -L/opt/local/lib/ -Ofast -o integerTetrahedrons integerTetrahedrons.c -lgmp
Use -ftrapv to check for integer overflow.
gcc -ftrapv -I/opt/local/include/ -L/opt/local/lib/ -Ofast -o integerTetrahedrons integerTetrahedrons.c -lgmp
gcc -Ofast -o integerTetrahedrons integerTetrahedrons.c -lgmp -fopenmp
gcc -Ofast -march=native -mtune=native -o integerTetrahedrons integerTetrahedrons.c -lgmp -fopenmp
   Laurence Park.

Makefile now exists, use that instead. Make sure the include and library paths are set.

*/

#include <stdio.h>
#include <unistd.h>
//#include <stdlib.h>
//#include <getopt.h>
//#include <ctype.h>
//#include <string.h>
//#include <unistd.h>

//#include <time.h>


#ifdef PARALLEL
#include <omp.h>
#endif

#include "tetrahedronChecks.h"
#include "tetrahedronOptions.h"


void tetrahedronSetSize(struct optionSet *options);
void tetrahedronSetDiameter(struct optionSet *options);
//void usage(void);





int main(int argc, char** argv) {

  struct optionSet options;
  struct optionSet *optionsp;
  optionsp = &options;

  /*
  int c;
  opterr = 0;
  int verboseFlag = 0;
  int diameterFlag = 0;
  int incrementalFlag = 0;
  long long sizeLower = -1;
  long long sizeUpper = -1;
  long long continuePos = -1;
  */

    // get commang line options
  optionsp = tetrahedronOptions(argc, argv, optionsp);
  printOptions(optionsp);

  initialiseOutput(optionsp);
 
  if (optionsp->diameterFlag == 0) {
    tetrahedronSetSize(optionsp);
  }
  if (optionsp->diameterFlag == 1) {
    tetrahedronSetDiameter(optionsp);
  }


  fclose(optionsp->outFile);
  return(0);
}


void tetrahedronSetSize(struct optionSet *options) {

  long long D01;
  long long tetrahedronSize;
  unsigned long long counter;
  long long M1lower;
  long long M1upper;
  long long M1by;

  /*
  FILE *checkpoint;
  char upperString[32];
  char lowerString[32];
  char checkpointName[1024] = "tetrahedronChechpoint.";

  sprintf(lowerString, "%lld", tetrahedronSizeLower);
  sprintf(upperString, "%lld", tetrahedronSizeUpper);
  strcat(checkpointName, lowerString);
  strcat(checkpointName, ".");
  strcat(checkpointName, upperString);

  // check if checkpoint file exists
  if(access(checkpointName, F_OK) != -1) {
    checkpoint = fopen(checkpointName, "rb");  // w for write, b for binary
    // file exists
    fclose(checkpoint);
  } else {
    // file doesn't exist
  }
  */

  if (options->verboseFlag) {
    fprintf(stderr, "Computing perimeters: %lli -- %lli\n", options->sizeLower, options->sizeUpper);
  }
#ifndef CHECK_DIFFERENCE
  if (options->incrementalFlag == 0) {
    fprintf(options->outFile, "Perimeter,Count\n", NULL);
  } else {
    fprintf(options->outFile, "Perimeter,Diameter,Count,Time\n", NULL);
  }
#endif
  
  for (tetrahedronSize = options->sizeLower; tetrahedronSize <= options->sizeUpper; tetrahedronSize++) {

    counter = 0;

    M1lower = ((tetrahedronSize - 1)/6) + 1;  /* integer division rounds down */
    M1upper = ((tetrahedronSize - 3)/3);
    if (tetrahedronSize <= 3) {
      M1upper = 1;
    }
    // start from given diameter (use when process is stopped during incremental).
    if (options->continuePos != -1) {
      M1lower = options->continuePos;
      options->continuePos = -1;
    }
    if (options->stopPos != -1) {
      M1upper = options->stopPos;
      options->stopPos = -1;
    }
    M1by = options->bySize;

    //fprintf(stderr, "Check: %lli %lli\n", M1lower, M1upper);
    D01 = M1lower;
    

    if (options->verboseFlag) {
      fprintf(stderr, "\r[ %lli ] Diameter: %lli / %lli -- Current Perimeter: %lli", tetrahedronSize, D01, M1upper, counter);
    }
    
#pragma omp parallel for reduction(+:counter) schedule(dynamic)
    for (D01 = M1lower; D01 <= M1upper; D01 += M1by) {

      unsigned long long delta;
      //clock_t start, end;
      double start = omp_get_wtime();
      //float seconds;

      //start = clock();
      
      //delta = countTetrahedronDiameterMP(tetrahedronSize, D01);
      //delta = countTetrahedronDiameter(tetrahedronSize, D01);
      //delta = countTetrahedronDiameterDouble(tetrahedronSize, D01);
      //delta = countTetrahedronDiameterZdouble(tetrahedronSize, D01);
      //counter += delta;

#ifdef PRECISION
      delta = countTetrahedronDiameterBasic(tetrahedronSize, D01);
      //delta = countTetrahedronDiameterOnlyCanon(tetrahedronSize, D01);
      //delta = countTetrahedronDiameterHybridMultiCheckCanonFirst(tetrahedronSize, D01);
      //delta = countTetrahedronDiameterHybridCanonFirst(tetrahedronSize, D01);
      //delta = countTetrahedronDiameterHybridMultiCheck(tetrahedronSize, D01);
      //delta = countTetrahedronDiameterHybrid(tetrahedronSize, D01);
#else
      delta = countTetrahedronDiameterZdouble(tetrahedronSize, D01);
#endif

      double end = omp_get_wtime();
      //end = clock();
      //seconds = (float)(end - start) / CLOCKS_PER_SEC;
      double seconds = end - start;
      
      counter += delta;

      if (options->verboseFlag) {
	fprintf(stderr, "\r[ %lli ] Diameter: %lli / %lli -- Current Perimeter: %lli", tetrahedronSize, D01, M1upper, counter);
      }
      if (options->incrementalFlag == 1) {
#ifdef CHECK_DIFFERENCE
	fprintf(options->outFile, "%lli,%lli\n", tetrahedronSize, D01);
#else
	fprintf(options->outFile, "%lli,%lli,%llu,%f\n", tetrahedronSize, D01, delta, seconds);
#endif
	fflush(options->outFile);
      }
    }
    if (options->verboseFlag) {
      fprintf(stderr, "\n", NULL);
    }
    if (options->incrementalFlag == 0) {
      fprintf(options->outFile, "%lli,%llu\n", tetrahedronSize, counter);
    }
    fflush(options->outFile);
  }
  
}



void tetrahedronSetDiameter(struct optionSet *options) {

  long long D01;
  long long tetrahedronDiameter;
  unsigned long long counter;
  long long M1lower;
  long long M1upper;
  long long M1by;
  
  if (options->verboseFlag) {
    fprintf(stderr, "Computing diameters: %lli -- %lli\n", options->sizeLower, options->sizeUpper);
  }

  if (options->incrementalFlag == 0) {
    fprintf(options->outFile, "Diameter,Count\n", NULL);
  } else {
    fprintf(options->outFile, "Diameter,Perimeter,Count,Time\n", NULL);
  }
  
  for (tetrahedronDiameter = options->sizeLower; tetrahedronDiameter <= options->sizeUpper; tetrahedronDiameter++) {

    counter = 0;

    M1lower = tetrahedronDiameter*3 + 3;
    M1upper = tetrahedronDiameter*6;
    // start from given size (use when process is stopped during incremental).
    if (options->continuePos != -1) {
      M1lower = options->continuePos;
      options->continuePos = -1;
    }
    if (options->stopPos != -1) {
      M1upper = options->stopPos;
      options->stopPos = -1;
    }
    M1by = options->bySize;
    
    D01 = M1lower;

    
    if (options->verboseFlag) {
      fprintf(stderr, "\r[ %lli ] Perimeter: %lli / %lli -- Current Count: %lli", tetrahedronDiameter, D01, M1upper, counter);
    }

#pragma omp parallel for reduction(+:counter) schedule(dynamic)
    for (D01 = M1lower; D01 <= M1upper; D01 += M1by) {

      unsigned long long delta;
      //clock_t start, end;
      //float seconds;
      double start = omp_get_wtime();

      //start = clock();
      
      //delta = countTetrahedronDiameterMP(tetrahedronSize, D01);
      //delta = countTetrahedronDiameter(tetrahedronSize, D01);
      //delta = countTetrahedronDiameterDouble(tetrahedronSize, D01);
      //delta = countTetrahedronDiameterZdouble(tetrahedronSize, D01);
      //counter += delta;
	
#ifdef PRECISION
      delta = countTetrahedronDiameterHybridMultiCheckCanonFirst(D01, tetrahedronDiameter);
      //delta = countTetrahedronDiameterHybridCanonFirst(D01, tetrahedronDiameter);
      //delta = countTetrahedronDiameterHybridMultiCheck(D01, tetrahedronDiameter);
      //delta = countTetrahedronDiameterHybrid(D01, tetrahedronDiameter);
#else
      delta = countTetrahedronDiameterZdouble(D01, tetrahedronDiameter);
#endif
      counter += delta;


      //end = clock();
      //seconds = (float)(end - start) / CLOCKS_PER_SEC;
      double end = omp_get_wtime();
      double seconds = end - start;

      
      if (options->verboseFlag) {
	fprintf(stderr, "\r[ %lli ] Perimeter: %lli / %lli -- Current Count: %lli", tetrahedronDiameter, D01, M1upper, counter);
      }
      if (options->incrementalFlag == 1) {
	fprintf(options->outFile, "%lli,%lli,%llu,%f\n", tetrahedronDiameter, D01, delta, seconds);
    	fflush(options->outFile);
      }


    }
    if (options->verboseFlag) {
      fprintf(stderr, "\n", NULL);
    }
    if (options->incrementalFlag == 0) {
      fprintf(options->outFile, "%lli,%llu\n", tetrahedronDiameter, counter);
    }
    fflush(options->outFile);
  }
  
}

