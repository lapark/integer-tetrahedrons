/* 
   Compute the number of integer length tetrahedrons.
   Compile using gcc -Ofast -o integerTetrahedrons integerTetrahedrons.c
gcc -I/opt/local/include/ -L/opt/local/lib/ -Ofast -o integerTetrahedrons integerTetrahedrons.c -lgmp
Use -ftrapv to check for integer overflow.
gcc -ftrapv -I/opt/local/include/ -L/opt/local/lib/ -Ofast -o integerTetrahedrons integerTetrahedrons.c -lgmp
gcc -Ofast -o integerTetrahedrons integerTetrahedrons.c -lgmp -fopenmp
gcc -Ofast -march=native -mtune=native -o integerTetrahedrons integerTetrahedrons.c -lgmp -fopenmp
   Laurence Park.

Makefile now exists, use that instead. Make sure the include and library paths are set.

*/

#include <stdio.h>
//#include <math.h>
#include <stdlib.h>

//#include <ctype.h>
#include <string.h>
//#include <unistd.h>

#include <time.h>

#include "tetrahedronChecks.h"
#include "tetrahedronOptions.h"

//#ifdef PARALLEL
//#include <omp.h>
//#endif

#include <mpi.h>



//void tetrahedronSetSize(long long tetrahedronSizeLower, long long tetrahedronSizeUpper, int verboseFlag, int incrementalFlag, long long continuePos);
//void tetrahedronSetDiameter(long long tetrahedronDiameterLower, long long tetrahedronDiameterUpper, int verboseFlag, int incrementalFlag, long long continuePos, struct optionSet *options);


void tetrahedronSetSizeMaster(struct optionSet *options);
void tetrahedronSetSizeSlave(struct optionSet *options);
void tetrahedronSetDiameterMaster(struct optionSet *options);
// A few functions that do the same calculations but with differing precision.








int main(int argc, char** argv) {


  //opterr = 0;

  struct optionSet options;
  struct optionSet *optionsp;
  optionsp = &options;
  
  int ierr;
  int id;
  int p;
  
  ierr = MPI_Init(&argc, &argv);
  
  ierr = MPI_Comm_size ( MPI_COMM_WORLD, &p ); // Get the number of processes.
  ierr = MPI_Comm_rank ( MPI_COMM_WORLD, &id ); // Get the individual process ID.

  // get command line options for all processes
  optionsp = tetrahedronOptions(argc, argv, optionsp);

  if (optionsp->incrementalFlag == 0) {
    if (id == 0) {
      fprintf(stderr, "Non-incremental mode is not supported.\n", id, p);
    }
    return(1);
  }

  if (id != 0) {
    // append ID to filename

    if (optionsp->fileName != NULL) {
      char *fileName = (char *) calloc(strlen(optionsp->fileName) + 16, sizeof(char));
      char *tid = (char *) calloc(16, sizeof(char));
      sprintf(tid, "%d", id);
      strcpy(fileName, optionsp->fileName);
      strcat(fileName, tid);
      free(optionsp->fileName);
      free(tid);
      optionsp->fileName = fileName;
    }
  }

  //open file once
  initialiseOutput(optionsp);
  
  if (id == 0) {

    //only run in master process
    printOptions(optionsp);

    fprintf(stderr, "P%d: The number of processes is %d.\n", id, p);
    
    if (optionsp->diameterFlag == 0) {
      tetrahedronSetSizeMaster(optionsp);
    }
    if (optionsp->diameterFlag == 1) {
      tetrahedronSetDiameterMaster(optionsp);
    }
  } else {
    tetrahedronSetSizeSlave(optionsp);
  }

  fclose(optionsp->outFile);
  ierr = MPI_Finalize();


  //fprintf(stderr,  "Output file: %s %d.\n", optionsp->fileName, id);
  
  return(0);
}



void tetrahedronSetSizeMaster(struct optionSet *options) {

  long long D01;
  long long tetrahedronSize;
  unsigned long long counter;
  long long M1lower;
  long long M1upper;

  long long sizeDiameter[2];
  
  int ierr, processCount = 0;
  int currentProcess;
  
  ierr = MPI_Comm_size(MPI_COMM_WORLD, &processCount); // Get the number of processes.

  //fprintf(stderr,  "Pmaster:    The number of processes is %d.\n", processCount );

  MPI_Status status;

  int currentlyComputing = 0;
  //unsigned long long delta;
  unsigned long long deltaDiameter[2];

  //unsigned long long tetrahedronSizes;
  
  /*
  FILE *checkpoint;
  char upperString[32];
  char lowerString[32];
  char checkpointName[1024] = "tetrahedronChechpoint.";

  sprintf(lowerString, "%lld", tetrahedronSizeLower);
  sprintf(upperString, "%lld", tetrahedronSizeUpper);
  strcat(checkpointName, lowerString);
  strcat(checkpointName, ".");
  strcat(checkpointName, upperString);

  // check if checkpoint file exists
  if(access(checkpointName, F_OK) != -1) {
    checkpoint = fopen(checkpointName, "rb");  // w for write, b for binary
    // file exists
    fclose(checkpoint);
  } else {
    // file doesn't exist
  }
  */

  if (options->verboseFlag) {
    fprintf(stderr, "Computing sizes: %lli -- %lli\n", options->sizeLower, options->sizeUpper);
  }
  if (options->incrementalFlag == 0) {
    fprintf(options->outFile, "Perimeter,Count\n", NULL);
  } else {
    fprintf(options->outFile, "Perimeter,Diameter,Count,Time\n", NULL);
  }
  
  for (tetrahedronSize = options->sizeLower; tetrahedronSize <= options->sizeUpper; tetrahedronSize++) {

    // debug
    //fprintf(stderr, "Top loop %d %d %d\n", tetrahedronSize, options->sizeLower, options->sizeUpper);

    
    counter = 0;

    M1lower = ((tetrahedronSize - 1)/6) + 1;  /* integer division rounds down */
    M1upper = ((tetrahedronSize - 3)/3);
    if (tetrahedronSize <= 3) {
      M1upper = 1;
    }
    // start from given diameter (use when process is stopped during incremental).
    if (options->continuePos != -1) {
      M1lower = options->continuePos;
      options->continuePos = -1;
    }
    //fprintf(stderr, "Check: %lli %lli\n", M1lower, M1upper);
    D01 = M1lower;
    

    if (options->verboseFlag) {
      fprintf(stderr, "\r[ %lli ] Diameter: %lli / %lli -- Current Count: %lli", tetrahedronSize, D01, M1upper, counter);
    }
    
    sizeDiameter[0] = tetrahedronSize;
    currentlyComputing = 0;

    // debug
    //fprintf(stderr, "Top loop2 %d %d %d\n", tetrahedronSize, options->sizeLower, options->sizeUpper);

    
    //#pragma omp parallel for reduction(+:counter) schedule(dynamic)
    //for (D01 = M1lower; (D01 <= M1upper) | (currentlyComputing > 0); D01++) {
    for (D01 = M1lower; D01 <= M1upper; D01++) {

      // wait for signal, then send next job to that process
      MPI_Recv(&deltaDiameter, 2, MPI_UNSIGNED_LONG_LONG, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
      //printf("[MPI master process] I received value %d, from rank %d.\n", deltaDiameter[1], status.MPI_SOURCE);

      if (deltaDiameter[1] > 0) {
	currentlyComputing--;
	counter += deltaDiameter[1];
      }
      
      if (options->verboseFlag) {
	//fprintf(stderr, "\r[ %lli | %d ] Diameter: %llu / %lli -- Current Size: %lli", tetrahedronSize, currentlyComputing, deltaDiameter[0], M1upper, counter);
	fprintf(stderr, "\r[ %lli | %d ] Diameter: %llu / %lli -- Current Count: %lli", tetrahedronSize, currentlyComputing, deltaDiameter[0], M1upper, counter);
      }

      if (D01 <= M1upper) {
	sizeDiameter[1] = D01;
      
	// master sends out diamters to compute and waits for counts
	// slave recevies diameters and returns count
	MPI_Send(&sizeDiameter, 2, MPI_LONG_LONG, status.MPI_SOURCE, 0, MPI_COMM_WORLD);
	currentlyComputing++;      
      }
    }


    fflush(options->outFile);
    if (options->verboseFlag) {
      fprintf(stderr, "\n", NULL);
    }
    if (options->incrementalFlag == 0) {
      fprintf(options->outFile, "%lli,%llu\n", tetrahedronSize, counter);
    }

  }


  // complete any leftovers
  while (currentlyComputing > 0) {
    // wait for signal, then send next job to that process
    MPI_Recv(&deltaDiameter, 2, MPI_UNSIGNED_LONG_LONG, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
    //printf("[MPI master process] I received value %d, from rank %d.\n", deltaDiameter[1], status.MPI_SOURCE);

    if (deltaDiameter[1] > 0) {
      currentlyComputing--;
      counter += deltaDiameter[1];
    }
      
    if (options->verboseFlag) {
      //fprintf(stderr, "\r[ %lli | %d ] Diameter: %llu / %lli -- Current Size: %lli", tetrahedronSize, currentlyComputing, deltaDiameter[0], M1upper, counter);
      fprintf(stderr, "\r[ %lli | %d ] Diameter: %llu / %lli -- Current Count: %lli", tetrahedronSize, currentlyComputing, deltaDiameter[0], M1upper, counter);
    }
  }


  //fprintf(stderr, "Exit loop!\n", NULL);

  // tell all processeses to end.
  for (currentProcess = 1; currentProcess < processCount; currentProcess++) {
    
    sizeDiameter[0] = 0;
    
    // master sends out diamters to compute and waits for counts
    // slave recevies diameters and returns count
    MPI_Send(&sizeDiameter, 2, MPI_LONG_LONG, currentProcess, 0, MPI_COMM_WORLD);
    
  }

  
  
}

void tetrahedronSetSizeSlave(struct optionSet *options) {
  
  long long sizeDiameter[2];
  unsigned long long deltaDiameter[2];
  
  int ierr, id;
  ierr = MPI_Comm_rank ( MPI_COMM_WORLD, &id ); // Get the individual process ID.

  clock_t start, end;
  float seconds;

  deltaDiameter[0] = 0;
  deltaDiameter[1] = 0;

  fprintf(stderr, "------------> P%d:  started.\n", id);

  // send ready signal
  MPI_Send(&deltaDiameter, 2, MPI_UNSIGNED_LONG_LONG, 0, 0, MPI_COMM_WORLD);
  // then wait for next job
  MPI_Recv(&sizeDiameter, 2, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  // until sent size 0
  deltaDiameter[0] = sizeDiameter[1];

  //fprintf(stderr,  "Output file: %s.\n", options->fileName);
  
  while(sizeDiameter[0] != 0) {

    start = clock();

    // debug
    //fprintf(stderr, "Counter: %lli\n", sizeDiameter[0]);
    
#ifdef PRECISION
    deltaDiameter[1] = countTetrahedronDiameterHybridMultiCheckCanonFirst(sizeDiameter[0], sizeDiameter[1]);
    //deltaDiameter[1] = countTetrahedronDiameterHybrid(sizeDiameter[0], sizeDiameter[1]);
#else
    deltaDiameter[1] = countTetrahedronDiameterZdouble(sizeDiameter[0], sizeDiameter[1]);
#endif

    end = clock();
    seconds = (float)(end - start) / CLOCKS_PER_SEC;

    MPI_Send(&deltaDiameter, 2, MPI_UNSIGNED_LONG_LONG, 0, 0, MPI_COMM_WORLD);


    //fprintf(stderr, "Incremental: %d\n", options->incrementalFlag);

    
    if (options->incrementalFlag == 1) {
      fprintf(options->outFile, "%lli,%lli,%llu,%f\n", sizeDiameter[0], sizeDiameter[1], deltaDiameter[1], seconds);
      fflush(options->outFile);
    }

    //fprintf(stderr, "------------> P%d:  waiting.\n", id);  
    MPI_Recv(&sizeDiameter, 2, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    //fprintf(stderr, "------------> P%d:  running.\n", id);  
    deltaDiameter[0] = sizeDiameter[1];
  }

  fprintf(stderr, "------------> P%d:  stopped.\n", id);  
}


/*
void tetrahedronSetDiameter(struct optionSet *options) {

  long long D01;
  long long tetrahedronDiameter;
  unsigned long long counter;
  long long M1lower;
  long long M1upper;

  if (options->verboseFlag) {
    fprintf(stderr, "Computing sizes: %lli -- %lli\n", options->sizeLower, options->sizeUpper);
  }

  if (options->incrementalFlag == 0) {
    fprintf(options->outFile, "Diameter,Count\n", NULL);
  } else {
    fprintf(options->outFile, "Diameter,Size,Count,Time\n", NULL);
  }
  
  for (tetrahedronDiameter = options->sizeLower; tetrahedronDiameter <= options->sizeUpper; tetrahedronDiameter++) {

    counter = 0;

    M1lower = tetrahedronDiameter*3 + 3;
    M1upper = tetrahedronDiameter*6;
    // start from given size (use when process is stopped during incremental).
    if (options->continuePos != -1) {
      M1lower = options->continuePos;
      options->continuePos = -1;
    }
    D01 = M1lower;

    
    if (options->verboseFlag) {
      fprintf(stderr, "\r[ %lli ] Perimeter: %lli / %lli -- Current Size: %lli", tetrahedronDiameter, D01, M1upper, counter);
    }

    //#pragma omp parallel for reduction(+:counter) schedule(dynamic)
    for (D01 = M1lower; D01 <= M1upper; D01++) {

      unsigned long long delta;
      clock_t start, end;
      float seconds;


      start = clock();
      
      //delta = countTetrahedronDiameterMP(tetrahedronSize, D01);
      //delta = countTetrahedronDiameter(tetrahedronSize, D01);
      //delta = countTetrahedronDiameterDouble(tetrahedronSize, D01);
      //delta = countTetrahedronDiameterZdouble(tetrahedronSize, D01);
      //counter += delta;
	
#ifdef PRECISION
      delta = countTetrahedronDiameterHybridMultiCheckCanonFirst(D01, tetrahedronDiameter);
      //delta = countTetrahedronDiameterHybrid(D01, tetrahedronDiameter);
#else
      delta = countTetrahedronDiameterZdouble(D01, tetrahedronDiameter);
#endif
      counter += delta;


      end = clock();
      seconds = (float)(end - start) / CLOCKS_PER_SEC;

      
      if (options->verboseFlag) {
	fprintf(stderr, "\r[ %lli ] Perimeter: %lli / %lli -- Current Size: %lli", tetrahedronDiameter, D01, M1upper, counter);
      }
      if (options->incrementalFlag == 1) {
	fprintf(options->outFile, "%lli,%lli,%llu\n", tetrahedronDiameter, D01, delta, seconds);
    	fflush(options->outFile);
      }


    }
    if (options->verboseFlag) {
      fprintf(stderr, "\n", NULL);
    }
    if (options->incrementalFlag == 0) {
      fprintf(options->outFile, "%lli,%llu\n", tetrahedronDiameter, counter);
    }
    fflush(options->outFile);
  }
  
}
*/



void tetrahedronSetDiameterMaster(struct optionSet *options) {

  long long D01;
  long long tetrahedronDiameter;
  unsigned long long counter;
  long long M1lower;
  long long M1upper;

  long long sizeDiameter[2];
  
  int ierr, processCount = 0;
  int currentProcess;
  
  ierr = MPI_Comm_size(MPI_COMM_WORLD, &processCount); // Get the number of processes.

  //fprintf(stderr,  "Pmaster:    The number of processes is %d.\n", processCount );

  MPI_Status status;

  int currentlyComputing = 0;
  //unsigned long long delta;
  unsigned long long deltaDiameter[2];

  //unsigned long long tetrahedronSizes;
  


  //fprintf(stderr,  "Output file: %s.\n", options->fileName);
  
  if (options->verboseFlag) {
    fprintf(stderr, "Computing sizes: %lli -- %lli\n", options->sizeLower, options->sizeUpper);
  }
  if (options->incrementalFlag == 0) {
    fprintf(options->outFile, "Count,Diameter\n", NULL);
  } else {
    fprintf(options->outFile, "Perimeter,Diameter,Count,Time\n", NULL);
  }
  
  for (tetrahedronDiameter = options->sizeLower; tetrahedronDiameter <= options->sizeUpper; tetrahedronDiameter++) {

    counter = 0;

    M1lower = tetrahedronDiameter*3 + 3;
    M1upper = tetrahedronDiameter*6;
    // start from given size (use when process is stopped during incremental).
    if (options->continuePos != -1) {
      M1lower = options->continuePos;
      options->continuePos = -1;
    }
    //fprintf(stderr, "Check: %lli %lli\n", M1lower, M1upper);
    D01 = M1lower;
    

    if (options->verboseFlag) {
      fprintf(stderr, "\r[ %lli ] Perimeter: %lli / %lli -- Current Count: %lli", tetrahedronDiameter, D01, M1upper, counter);
    }

    sizeDiameter[0] = 0; //<--checkme
    sizeDiameter[1] = tetrahedronDiameter; //<--checkme
    currentlyComputing = 0;
    
    //#pragma omp parallel for reduction(+:counter) schedule(dynamic)
    for (D01 = M1lower; D01 <= M1upper; D01++) {


      // passing sizeDiameter[2] containing { perimeter, diameter }
      // passing deltaDiameter[2] containing { diameter, count for perimeter,diameter }
      
      // wait for signal, then send next job to that process
      MPI_Recv(&deltaDiameter, 2, MPI_UNSIGNED_LONG_LONG, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
      //printf("[MPI master process] I received value %d, from rank %d.\n", deltaDiameter[1], status.MPI_SOURCE);

      if (deltaDiameter[1] > 0) {
	currentlyComputing--;
	counter += deltaDiameter[1];
      }
      
      if (options->verboseFlag) {
	fprintf(stderr, "\r[ %lli | %d ] Perimeter: %llu / %lli -- Current Count: %lli", tetrahedronDiameter, currentlyComputing, sizeDiameter[0], M1upper, counter);
      }

      if (D01 <= M1upper) {
	sizeDiameter[0] = D01;
      
	// master sends out diamters to compute and waits for counts
	// slave recevies diameters and returns count
	MPI_Send(&sizeDiameter, 2, MPI_LONG_LONG, status.MPI_SOURCE, 0, MPI_COMM_WORLD);
	currentlyComputing++;      
      }
    }


    fflush(options->outFile);
    if (options->verboseFlag) {
      fprintf(stderr, "\n", NULL);
    }
    if (options->incrementalFlag == 0) {
      fprintf(options->outFile, "%lli,%llu\n", tetrahedronDiameter, counter);
    }

  }

  while (currentlyComputing > 0) {

    // wait for signal, then send next job to that process
    MPI_Recv(&deltaDiameter, 2, MPI_UNSIGNED_LONG_LONG, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
    //printf("[MPI master process] I received value %d, from rank %d.\n", deltaDiameter[1], status.MPI_SOURCE);

    if (deltaDiameter[1] > 0) {
      currentlyComputing--;
      counter += deltaDiameter[1];
    }
      
    if (options->verboseFlag) {
      fprintf(stderr, "\r[ %lli | %d ] Perimeter: %llu / %lli -- Current Count: %lli", tetrahedronDiameter, currentlyComputing, sizeDiameter[0], M1upper, counter);
    }
  }


  // tell all processeses to end.
  for (currentProcess = 1; currentProcess < processCount; currentProcess++) {
    
    sizeDiameter[0] = 0;
    
    // master sends out diamters to compute and waits for counts
    // slave recevies diameters and returns count
    MPI_Send(&sizeDiameter, 2, MPI_LONG_LONG, currentProcess, 0, MPI_COMM_WORLD);
    
  }
  
}
