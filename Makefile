#!/usr/bin/make -f

INCLUDE = -I/opt/local/include/ -I/opt/local/include/libomp/
DEFS = -DPRECISION -DPARALLEL #-DCHECK_DIFFERENCE
#DEFS = -DPARALLEL
#DEFS = 
#DEFS = CHECK_LARGEST # for examining size of tetrahedron check

CC = gcc
#CC = /opt/local/bin/gcc-mp-8

## mp compiler for OMP
## mpi compiler for MPI
#CC = gcc-mp-11
#MPICC = /opt/local/bin/mpicc
MPICC = /opt/local/bin/mpicc-openmpi-gcc8

CFLAGS = $(INCLUDE) -Ofast -march=native -mtune=native $(DEFS)
MPICFLAGS = $(INCLUDE) -Ofast -march=native -mtune=native 

LIBPATH = -L/opt/local/lib/ -L/opt/local/lib/libomp/
LIBS = $(LIBPATH) -lgmp -fopenmp
MPILIBS = $(LIBPATH) -lgmp


plain: integerTetrahedrons

mpi: integerTetrahedronsMPI


integerTetrahedrons:	integerTetrahedrons.o tetrahedronChecks.o tetrahedronOptions.o
	$(CC) $(INCLUDE) -o integerTetrahedrons integerTetrahedrons.o tetrahedronChecks.o tetrahedronOptions.o $(LIBS) 


integerTetrahedronsMPI:	integerTetrahedronsMPI.o tetrahedronChecks.o tetrahedronOptions.o
	$(MPICC) $(INCLUDE) -o integerTetrahedronsMPI integerTetrahedronsMPI.o tetrahedronChecks.o tetrahedronOptions.o $(MPILIBS)


integerTetrahedronsMPI.o: integerTetrahedronsMPI.c
	$(MPICC) $(INCLUDE) $(MPICFLAGS) -c integerTetrahedronsMPI.c


integerTetrahedrons.o: integerTetrahedrons.c
	$(CC) $(INCLUDE) $(CFLAGS) -fopenmp -c integerTetrahedrons.c


tetrahedronChecks.o:	tetrahedronChecks.c
	$(CC) $(INCLUDE) $(CFLAGS) -c tetrahedronChecks.c

tetrahedronOptions.o:	tetrahedronOptions.c
	$(CC) $(INCLUDE) $(CFLAGS) -c tetrahedronOptions.c

clean:
	rm integerTetrahedrons.o integerTetrahedronsMPI.o tetrahedronChecks.o
