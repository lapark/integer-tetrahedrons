

// A few functions that do the same calculations but with differing precision.
unsigned long long countTetrahedronDiameter(long long tetrahedronSize, long long D01);
unsigned long long countTetrahedronDiameterDouble(long long tetrahedronSize, long long D01);
unsigned long long countTetrahedronDiameterZdouble(long long tetrahedronSize, long long D01);

#ifdef PRECISION
unsigned long long countTetrahedronDiameterMP(long long tetrahedronSize, long long D01);
unsigned long long countTetrahedronDiameterHybrid(long long tetrahedronSize, long long D01);
unsigned long long countTetrahedronDiameterHybridMultiCheck(long long tetrahedronSize, long long D01);
unsigned long long countTetrahedronDiameterHybridCanonFirst(long long tetrahedronSize, long long D01);
unsigned long long countTetrahedronDiameterHybridMultiCheckCanonFirst(long long tetrahedronSize, long long D01);
#endif

unsigned long long countTetrahedronDiameterOnlyCanon(long long tetrahedronSize, long long D01);

unsigned long long countTetrahedronDiameterBasic(long long tetrahedronSize, long long D01);

int checkCanonicalVector(long long *D);
long double dellaFrancesca(const long long *DS);
long double jamesEastC6(const long long *DS);
