
// structure used for options
struct optionSet {
  int verboseFlag;
  int diameterFlag;
  int incrementalFlag;
  long long sizeLower;
  long long sizeUpper;
  long long continuePos;
  long long stopPos;
  long long bySize;
  char *fileName;
  FILE *outFile;
};



void usage(void);
struct optionSet *tetrahedronOptions(int argc, char * const *argv, struct optionSet *options);
void printOptions(struct optionSet *options);
void initialiseOutput(struct optionSet *optionsp);
