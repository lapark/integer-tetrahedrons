#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>

#include "tetrahedronOptions.h"


void printOptions(struct optionSet *options) {

  if (options->diameterFlag == 1) {
    fprintf (stderr, "-- Computing the number of tetrahedrons with diameter between %d and %d.\n",
	     options->sizeLower, options->sizeUpper);
    if (options->incrementalFlag == 1) {
      fprintf (stderr, "-- Incremental (printing each perimeter).\n");
    }
  } else {
    fprintf (stderr, "-- Computing the number of tetrahedrons with perimeter between %d and %d.\n",
	     options->sizeLower, options->sizeUpper);
    if (options->incrementalFlag == 1) {
      fprintf (stderr, "-- Incremental (printing each diameter).\n");
    }
  }

  if (options->continuePos != -1) {
    fprintf (stderr, "-- Continuing from %d.\n", options->continuePos);
  }

  if (options->fileName != NULL) {
    fprintf (stderr, "-- Writing results to %s.\n", options->fileName);
  } else {
    fprintf (stderr, "-- Writing results to stdout.\n");
  }
}

struct optionSet *tetrahedronOptions(int argc, char * const *argv, struct optionSet *options) {

  int c;
  char fileName[128];
  
  static struct option longopts[] = {
    { "verbose",     no_argument,       NULL, 'v' },
    { "diameter",    no_argument,       NULL, 'd' },
    { "perimeter",   no_argument,       NULL, 'p' },
    { "continue",    required_argument, NULL, 'c' },
    { "stop",        required_argument, NULL, 's' },
    { "by",          required_argument, NULL, 'b' },      
    { "incremental", no_argument,       NULL, 'i' },
    { "help",        no_argument,       NULL, 'h'  },
    { "output",      required_argument, NULL, 'o' },
    { "lower",       required_argument, NULL, 'l' },
    { "upper",       required_argument, NULL, 'u' },
    { NULL,          0,                 NULL,  0  }
  };

  // set defaults
  options->verboseFlag = 0;
  options->diameterFlag = 0;
  options->incrementalFlag = 0;
  options->sizeLower = -1;
  options->sizeUpper = -1;
  options->continuePos = -1;
  options->stopPos = -1;
  options->bySize = 1;
  options->fileName = NULL;
  options->outFile = stdout;

  
  while ((c = getopt_long (argc, argv, "vdphl:u:ic:s:b:o:", longopts, NULL)) != -1) {
    switch (c) {
    case 'v':
      options->verboseFlag = 1;
      break;
    case 'd':
      options->diameterFlag = 1;
      break;
    case 'p':
      options->diameterFlag = 0;
      break;
    case 'c':
      options->continuePos = strtoul(optarg, NULL, 10);
      break;
    case 's':
      options->stopPos = strtoul(optarg, NULL, 10);
      break;
    case 'b':
      options->bySize = strtoul(optarg, NULL, 10);
      break;
    case 'i':
      options->incrementalFlag = 1;
      break;
    case 'h':
      usage();
      exit(0);
    case 'o':
      options->fileName = strdup(optarg);
      break;
    case 'l':
      options->sizeLower = strtoul(optarg, NULL, 10);
      break;
    case 'u':
      options->sizeUpper = strtoul(optarg, NULL, 10);
      break;
    case '?':
      if ((optopt == 'l') | (optopt == 'u')) {
	fprintf (stderr, "Option -%c requires an argument.\n", optopt);
	usage();
      }
      else if (isprint (optopt)) {
	fprintf (stderr, "Unknown option `-%c'.\n", optopt);
	usage();
      }
      else {
	fprintf (stderr,
		 "Unknown option character `\\x%x'.\n",
		 optopt);
      }
    default:
      usage();
      abort();
    }
  }

  // check values
  if (options->sizeLower == -1) {
    fprintf (stderr, "Error: Please provide a lower tetrehedron size.\n", NULL);
    exit(1);
  }
  if (options->sizeUpper == -1) {
    options->sizeUpper = options->sizeLower;
  }
  if (options->sizeUpper < options->sizeLower) {
    fprintf (stderr, "Error: Please ensure upper tetrehedron size is at least as large as the lower tetrehedron size.\n", NULL);
    exit(1);
  }
  
  return(options);

}


void usage(void) {
  fprintf (stderr, "Calculate the number of tetrahedrons with given perimeter.\n", NULL);
  fprintf (stderr, "Usage: integerTetrahedrons [--verbost] [--diameter] --lower=lowerSize [--upper=upperSize] [--incremental] [--continue position] [--output=filename]\n", NULL);
  fprintf (stderr, "\t--verbose: Verbose output\n", NULL);
  fprintf (stderr, "\t--incremental: Incremental output\n", NULL);
  fprintf (stderr, "\t--diameter: Calculate number of tetrahedrons of a given diameter (rather than perimeter).\n", NULL);
  fprintf (stderr, "\t--output=filename: file to save results\n", NULL);
  fprintf (stderr, "\t--lower=size: Lower size [Required]\n", NULL);
  fprintf (stderr, "\t--upper=size: Upper size [If ommited, assumed to equal the lower size]\n", NULL);
  fprintf (stderr, "\t--continue=size: Continue from intermediate position.\n", NULL);
  fprintf (stderr, "\t--stop=size: Stop at intermediate position.\n", NULL);
  fprintf (stderr, "\t--by=size: increment size [default 1].\n", NULL);
}


void initialiseOutput(struct optionSet *optionsp) {
  
  // open file pointer if requested
  if (optionsp->fileName != NULL) {
    if (access(optionsp->fileName, F_OK) == 0 ) {
      // file exists
      fprintf(stderr, "File %s exists!\n", optionsp->fileName);
      exit(1);
    } 
    optionsp->outFile = fopen(optionsp->fileName, "w");
    if (optionsp->outFile == NULL) {
      fprintf(stderr, "Error opening file!\n");
      exit(1);
    }
  }  
}
